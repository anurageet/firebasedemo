//
//  ILGeneralApi.swift
//  firebaseDemo
//
//  Created by Anurag Bhakuni on 11/09/19.
//  Copyright © 2019 Gaurav Chopra. All rights reserved.
//

import Foundation

import AFNetworking

enum apiType
{
    case listing
    case comments
}

class ILGeneralApi {
   
    var apiName : apiType!;
   
   typealias fTypeAlias = (_ error :Error) -> ()
   typealias sTypeAlias = (_ responseTask : URLSessionDataTask, _ responseDictionary : Any ) -> ()
    var successBlock : sTypeAlias?;
    var failure :   fTypeAlias?

    
    
    
    func hitapiwithType(apiname: apiType,params:Int=0, success : @escaping sTypeAlias,failure : @escaping fTypeAlias  )  {
        
        self.apiName = apiname;
        
        self.successBlock = success
        self.failure = failure
        switch self.apiName {
        case .listing?:
            self.hitGetApi(apiName: APIFIREBASEISSUE)
            break
        case .comments?:
            self.hitGetApi(apiName: String(format: "%@%d/comments", APIFIREBASECOMMENT,params) )
            break;
        default:
            
            break
            
        }
        
    }

    func hitGetApi(apiName : String)  {
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.get(apiName, parameters: nil, progress: nil, success: { (task: URLSessionDataTask!, responseObject: Any!) in
            if let response = responseObject
            {
                self.successBlock!(task,response);
            }
            
        }) { (task: URLSessionDataTask!, error: Error!) in
            self.failure!(error)
        }
        

        
    }
    
    
    
    
}



