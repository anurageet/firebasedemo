//
//  ILDefineSwift.swift
//  firebaseDemo
//
//  Created by Anurag Bhakuni on 11/09/19.
//  Copyright © 2019 Gaurav Chopra. All rights reserved.
//

import Foundation

class ILDefineSwift  {
    
    public class func optionalHandling<T>(_param: T!, _returnType: T.Type) -> T {
        
        
        if let value = _param {
            return value
        }
        else if _returnType is String.Type {
            return "" as! T
        }else if _returnType is Int.Type {
            return 0 as! T
        }else if _returnType is Bool.Type {
            return false as! T
        }else if _returnType is Int64.Type {
            return 0 as! T
        }else{
            return "" as! T
        }
    }
    
}
