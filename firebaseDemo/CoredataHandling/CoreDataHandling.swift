//
//  CoreDataHandling.swift
//  firebaseDemo
//
//  Created by Anurag Bhakuni on 10/09/19.
//  Copyright © 2019 Gaurav Chopra. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataHandling {
    var appDelegate : AppDelegate!
    var context : NSManagedObjectContext!

    init() {
        appDelegate = UIApplication.shared.delegate as? AppDelegate
         context = appDelegate.persistentContainer.viewContext;
    }
    
    func insertIssue(dictionaryJson : Array<Any>)  {
      
       self.insertIssueList(issueArrSwift: JSON(dictionaryJson), context: context)
        let date = Date()
        UserDefaults.standard.set(date, forKey: KDATECOREDATASAVE);
        
    }
    func insertIssueList(issueArrSwift : JSON, context : NSManagedObjectContext)
    {
        do {
            
            
            for issueObject in issueArrSwift{
                let issueDetail: IssueList =  NSEntityDescription.insertNewObject(forEntityName: "IssueList", into: context) as! IssueList;
                issueDetail.id = ILDefineSwift.optionalHandling(_param: issueObject.1.dictionary?[APINUMBERID]?.int64, _returnType: Int64.self)
                issueDetail.title = ILDefineSwift.optionalHandling(_param: issueObject.1.dictionary?[APITITLEISSUEID]?.string, _returnType: String.self)
               var description = ILDefineSwift.optionalHandling(_param: issueObject.1.dictionary?[APITITLEBODYID]?.string, _returnType: String.self)
                issueDetail.updateDate = ILDefineSwift.optionalHandling(_param: issueObject.1.dictionary?[APIUPDATEDID]?.string, _returnType: String.self)

                
                if description.count >= 140
                {
                    description = String(description.prefix(140))
                }
                issueDetail.descript = description
              
                   try context.save();
            }
      
        } catch  {
            print("Exception");
        }
   
    }
    
   
    
    func insertComment(issueArrSwift : JSON, withCommentID : Int64)
    {
        do {
            
            
            for commentObject in issueArrSwift{
                let commentDetail: Comment =  NSEntityDescription.insertNewObject(forEntityName: "Comment", into: context) as! Comment;
                commentDetail.comment = ILDefineSwift.optionalHandling(_param: commentObject.1.dictionary?[APITITLEBODYID]?.string, _returnType: String.self)
                commentDetail.id = withCommentID
                commentDetail.name = ILDefineSwift.optionalHandling(_param: commentObject.1.dictionary?[APITITLEUSERID]?.dictionary?[APITITLELOGINID]?.string , _returnType: String.self)
                try context.save();
            }
            
        } catch  {
            print("Exception");
        }
        
    }
    
    
    func commentCheck(productId : Int) -> Result<Int32, NSError> {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Comment")
        do {
            fetchRequest.predicate = NSPredicate(format: "id == %d", productId)
            
            let results:[Comment] = try ((context.fetch(fetchRequest) as? [Comment])!)
            
            if results.count > 0
            {
                return .success(1)
            }
            return .success(-1)
            
        } catch let error as NSError {
            
            return .failure(error)
        }
        
    }
    
    func issueCheck() -> Result<Int32, NSError> {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "IssueList")
        do {
            let results:[IssueList] = try ((context.fetch(fetchRequest) as? [IssueList])!)
            if results.count > 0
            {
                return .success(1)
            }
            return .success(-1)
            
        } catch let error as NSError {
            
            return .failure(error)
        }
        
    }
    
    
    
    func purgeData()  {
        
        let objUserdetailFetch = NSFetchRequest<NSFetchRequestResult>.init();
        objUserdetailFetch.entity = NSEntityDescription.entity(forEntityName: "IssueList", in: context)
        objUserdetailFetch.includesSubentities = false;
        do {
            let results:[IssueList] = try ((context.fetch(objUserdetailFetch) as? [IssueList])!)
            for manageObject in results
            {
                   context.delete(manageObject)
                    self.saveManageObject()
            }
        } catch  {
            
        }
        
        
        let objUserdetailFetch1 = NSFetchRequest<NSFetchRequestResult>.init();
        objUserdetailFetch1.entity = NSEntityDescription.entity(forEntityName: "Comment", in: context)
        objUserdetailFetch1.includesSubentities = false;
        do {
            let results1:[Comment] = try ((context.fetch(objUserdetailFetch1) as? [Comment])!)
            for manageObject in results1
            {
                context.delete(manageObject)
                self.saveManageObject()
            }
        } catch  {
            
        }
        
    }
    func saveManageObject()
    {
        do {
            try context.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    
    func fetchIssueList() -> [IssueList]?  {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "IssueList")
        do {
            let results:[IssueList] = try (context.fetch(fetchRequest) as? [IssueList])!
            let sort = NSSortDescriptor(key: "", ascending: true)
            fetchRequest.sortDescriptors = [sort]
            return results
        } catch _ as NSError {
        }
        return nil
    }
   
    func fetchCommentDetail(productId : Int) -> [Comment]?  {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Comment")
        do {
            fetchRequest.predicate = NSPredicate(format: "id == %d", productId)
            let results:[Comment] = try (context.fetch(fetchRequest) as? [Comment])!
            return results
        } catch _ as NSError {
        }
        return nil
        
    }
    
    
    
    
    
}
