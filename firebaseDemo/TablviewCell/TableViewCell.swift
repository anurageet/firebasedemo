//
//  TableViewCell.swift
//  firebaseDemo
//
//  Created by Anurag Bhakuni on 10/09/19.
//  Copyright © 2019 Gaurav Chopra. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var txtIssue: UILabel!
    
    
    func customize(isseModal : IssueList)  {
        
        let str1 : NSAttributedString = NSAttributedString.init(string: String(format: " Title :- %@", isseModal.title!), attributes: [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)]);
        
        let str2 : NSAttributedString = NSAttributedString.init(string: String(format: "\n Description :- %@", isseModal.descript!), attributes: [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 12)]);
        
        let heading : NSMutableAttributedString = NSMutableAttributedString.init()
        
        heading.append(str1)
        heading.append(str2)
        
        self.txtIssue.attributedText = heading
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
