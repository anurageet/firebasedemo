//
//  CommentTableViewCell.swift
//  firebaseDemo
//
//  Created by Anurag Bhakuni on 12/09/19.
//  Copyright © 2019 Gaurav Chopra. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var lbelComment: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func customize(commentModal : Comment)  {
        
        let str1 : NSAttributedString = NSAttributedString.init(string: String(format: " Name :- %@", commentModal.name!), attributes: [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)]);
        
        let str2 : NSAttributedString = NSAttributedString.init(string: String(format: "\n Comment :- %@", commentModal.comment!), attributes: [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 12)]);
        
        let heading : NSMutableAttributedString = NSMutableAttributedString.init()
        
        heading.append(str1)
        heading.append(str2)
        
        self.lbelComment.attributedText = heading
        
    }
    
    
}
