//
//  ViewController.swift
//  firebaseDemo
//
//  Created by Anurag Bhakuni on 10/09/19.
//

import UIKit

let APIFIREBASEISSUE = "https://api.github.com/repos/firebase/firebase-ios-sdk/issues"
let APIFIREBASECOMMENT = "https://api.github.com/repos/firebase/firebase-ios-sdk/issues/"

let APINUMBERID = "number"
let APITITLEISSUEID = "title"
let APITITLEBODYID = "body"
let APITITLELOGINID = "login"
let APITITLEUSERID = "user"
let APIUPDATEDID = "updated_at"
let KDATECOREDATASAVE = "DATECOREDATASAVE"



class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tblview: UITableView!
    
    var arrListing : Array<Any>?;
     var objCoreDataHandling = CoreDataHandling.init();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.yellow
        navigationItem.title = "Firebase Demo"
        self.tblview.isHidden = true;
        
      
        tblview.register(UINib.init(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        tblview.delegate = self;
        tblview.dataSource = self;
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        
        let result = objCoreDataHandling.issueCheck();

        switch result {
        case .success(let id):
            if id == 1
            {
                let date = Date();
                let dateStore = UserDefaults.standard.value(forKey: KDATECOREDATASAVE);
                let calendar = Calendar.current
                let components = calendar.dateComponents([.year, .month, .day], from: date)
                
                let day = components.day
                
                let components1 = calendar.dateComponents([.year, .month, .day], from: dateStore as! Date)
                let dayS = components1.day
                
                if dayS! - day! >= 1
                {
                    objCoreDataHandling.purgeData();
                    self.apiHitting()
                }
                else
                {
                     self.feedingArrFromCoredata()
                }
            }
            else
            {
                self.apiHitting()
            }
            break;
        case .failure( _) :
            self.apiHitting()
              break;
        default:
            break;
        }
        
        
    
    }
    
    
    
    
    
    func feedingArrFromCoredata()
    {
        arrListing =   objCoreDataHandling.fetchIssueList();
        if let arrComm = arrListing
        {
            if arrComm.count <= 0
            {
                let alert = UIAlertController(title: "", message: "NO issue", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    alert.dismiss(animated: false, completion: nil)
                    
                }))
                
                self.present(alert, animated: true)
            }
            else
            {
                self.tblview.isHidden = false;

            }
            
            
        }
      
        

        self.tblview.reloadData();
        
    }
    
    
    func feedingCoreData(dictionary : Array<Any>)
    {
            objCoreDataHandling.insertIssue(dictionaryJson: dictionary);
        
    }
    
    func apiHitting()
    {
        let generalApi = ILGeneralApi.init();
        generalApi.hitapiwithType(apiname: .listing, success: { (responseTask, responseDictionary) in
            
            if let arrayResponse  = responseDictionary as? [Any] {
                
                if arrayResponse.count > 0
                {
                    self.feedingCoreData(dictionary: arrayResponse);
                    self.feedingArrFromCoredata()
                    print(responseDictionary);
                }
                
                
            }
            
           
            
        }, failure: { (error) in
            
        });
    }
    
    
    //MARK: tableview delegates
    
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let commentView = CommentListingViewController.init(nibName: "CommentListingViewController", bundle: nil);
        commentView.isseueId = (arrListing![indexPath.row] as! IssueList).id
        self.navigationController?.pushViewController(commentView, animated: false);
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let arrCount = arrListing
        {
            return arrCount.count;
        }
        else
        {
            return 0 ;
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        cell.customize(isseModal : arrListing![indexPath.row] as! IssueList);
        
        return cell
        
    }

    
    
    
    

}

