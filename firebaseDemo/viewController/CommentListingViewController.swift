//
//  CommentListingViewController.swift
//  firebaseDemo
//
//  Created by Anurag Bhakuni on 12/09/19.
//  Copyright © 2019 Gaurav Chopra. All rights reserved.
//

import UIKit


class CommentListingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblview: UITableView!
    var arrComment : Array<Any>?;
    
    var isseueId : Int64 = 0
    var objCoreDataHandling = CoreDataHandling.init();
    override func viewDidLoad() {
        super.viewDidLoad()

        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.yellow
        navigationItem.title = "Firebase Demo"
        tblview.register(UINib.init(nibName: "CommentTableViewCell", bundle: nil), forCellReuseIdentifier: "CommentTableViewCell")
        tblview.delegate = self;
        tblview.dataSource = self;
    }

    override func viewDidAppear(_ animated: Bool) {
        
        let result = objCoreDataHandling.commentCheck(productId: Int(isseueId));
        
        switch result {
        case .success(let id):
            if id == 1
            {
                    self.feedingArrFromCoredata()
            }
            else
            {
                self.apiHitting()
            }
            break;
        case .failure( _) :
            self.apiHitting()
            break;
        default:
            break;
        }
        
    }
    
    func feedingArrFromCoredata()
    {
        arrComment =   objCoreDataHandling.fetchCommentDetail(productId: Int(isseueId));
        if let arrComm = arrComment
        {
            if arrComm.count <= 0
            {
                let alert = UIAlertController(title: "", message: "NO comments", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    alert.dismiss(animated: false, completion: nil)
                    
                }))
                
                self.present(alert, animated: true)
            }
            else
            {
                self.tblview.isHidden = false;
                
            }
            
        }
        
        self.tblview.reloadData();
    }
    
    func feedingCoreData(dictionary : Array<Any>)
    {
        objCoreDataHandling.insertComment(issueArrSwift: JSON (dictionary), withCommentID:isseueId);
    }
    
    func apiHitting()
    {
        let generalApi = ILGeneralApi.init();
        generalApi.hitapiwithType(apiname: .comments, params : Int(isseueId), success: { (responseTask, responseDictionary) in
            
            if let arrayResponse  = responseDictionary as? [Any] {
                
                if arrayResponse.count > 0
                {
                    self.feedingCoreData(dictionary: arrayResponse);
                    self.feedingArrFromCoredata()
                    print(responseDictionary);
                }
                else
                {
                    let alert = UIAlertController(title: "", message: "NO comments", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        alert.dismiss(animated: false, completion: nil)
                        
                    }))
                    
                    self.present(alert, animated: true)
                }
            }
        }, failure: { (error) in
        });
    }
    
    
    
    
    //MARK: tableview delegates
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let arrCount = arrComment
        {
            return arrCount.count;
        }
        else
        {
            return 0 ;
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
        cell.customize(commentModal : arrComment![indexPath.row] as! Comment);
        
        return cell
        
    }


}
