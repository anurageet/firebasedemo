//
//  IssueModals.swift
//  firebaseDemo
//
//  Created by Anurag Bhakuni on 10/09/19.
//  Copyright © 2019 Gaurav Chopra. All rights reserved.
//

import Foundation

struct IssueModals: Codable {
    let data: [Int]?
    let info: Int?
}
